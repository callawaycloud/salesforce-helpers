/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Utility methods for scheduling.
*/
public class SchedulingHelper {

	public static String dateTimeToCron(DateTime input) {
		return input.second()
			+ ' ' + input.minute()
			+ ' ' + input.hour()
			+ ' ' + input.day()
			+ ' ' + input.month()
			+ ' ?'
			+ ' ' + input.year();
	}

	@isTest
	private static void testDateTimeToCron() {
		DateTime input = DateTime.newInstance(2006, 1, 1, 1, 1, 1);
		system.assertEquals('1 1 1 1 1 ? 2006', dateTimeToCron(input));
	}

}