/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Utility methods for dynamic SOQL
*/
public class DynamicSOQLHelper {

	/* Constants */
	
	public static final String NULL_LIST = '(\'\')';
	public static final String NULL_STRING = '\'\'';

	/* Primary Formatting Helpers */
	
	// escapes single quotes and wraps with quotes
	public static String format(String input) {
		if(input == null)
			return NULL_STRING;
		return '\'' + String.escapeSingleQuotes(input) + '\''; 
	}
	
	// returns YYYY-MM-DD
	public static String format(Date input) {
		return input.year() 
			+ '-' + (input.month() < 10 ? '0' : '') + input.month() 
			+ '-' + (input.day() < 10 ? '0' : '') + input.day();
	}

	// returns ('value1','value2')
	// filters out duplicate values (case sensitive)
	public static String format(List<String> input) {
		Set<String> inputSet = new Set<String>();
		inputSet.addAll(input);
		return format(inputSet);	
	}
	
	// returns ('value1','value2')
	public static String format(Set<String> input) {
		if(input == null || input.isEmpty())
			return '(\'\')';
		
		String temp = '';
		for(Object value : input) {
			temp += ',\'' + String.valueOf(value) + '\'';
		}
		return '(' + temp.subString(1) + ')';
	}
	
	/* Secondary Formatting Helpers */
	// functions that just cast argument into string type and call primary formatting function
	
	public static String format(Id input) {
		String temp = input;
		return format(temp);
	}
	
	public static String format(List<Id> input) {
		List<String> temp = new List<String>();
		for(Id value : input) {
			temp.add(value);
		}
		return format(temp); 
	}
	
	public static String format(Set<Id> input) {
		Set<String> temp = new Set<String>();
		for(Id value : input) {
			temp.add(value);
		}
		return format(temp);
	}
	
	/* Deprecated Methods */
	
	// Retained for backwards compatibility with older versions of this helper.  Delete
	// this section if deploying to a new org
	public static String formatDateForSOQL(Date input) { return format(input); }
	public static String formatListForSOQL(List<String> input) { return format(input); }
	public static String formatSetForSOQL(Set<String> input) { return format(input); }
	
	/* Test Methods */
	
	private static final Id TEST_ID = '001V0000009oBXSIA2';
	
	@isTest
	private static void testListFormat() {
		String order1 = '(\'asdf\',\'qwer\')';
		String order2 = '(\'qwer\',\'asdf\')';
		String output = format(new String[] { 'asdf','asdf','qwer','qwer' });
		system.assert(output == order1 || output == order2, output);
	}
	
	@isTest
	private static void testNullListFormat() {
		system.assertEquals(NULL_LIST, format(new List<String>()));
	}
	
	@isTest
	private static void testSetFormat() {
		Set<String> input = new Set<String>();
		input.add('asdf');
		input.add('qwer');
		String order1 = '(\'asdf\',\'qwer\')';
		String order2 = '(\'qwer\',\'asdf\')';
		String output = format(input);
		system.assert(output == order1 || output == order2, output);
	}
	
	@isTest
	private static void testNullSetFormat() {
		system.assertEquals(NULL_LIST, format(new Set<String>()));
	}
	
	@isTest
	private static void testDateFormat() {
		system.assertEquals(
			  '2011-01-01'
			, format(Date.newInstance(2011, 1, 1))
		);
	}
	
	@isTest
	private static void testStringFormat() {
		system.assertEquals(
 			  '\'a\\\'a\''
			, format('a\'a')
		);
	}
	
	@isTest
	private static void testNullStringFormat() {
		String empty;
		system.assertEquals(NULL_STRING, format(empty));
	}
	
	@isTest
	private static void testIdFormat() {
		system.assertEquals('\'' + TEST_ID + '\'', format(TEST_ID));
	}
	
	@isTest
	private static void testSetIdFormat() {
		system.assertEquals('(\'' + TEST_ID + '\')', format(new Set<Id>{ TEST_ID }));
	}
	
	@isTest
	private static void testListIdFormat() {
		system.assertEquals('(\'' + TEST_ID + '\')', format(new List<Id>{ TEST_ID, TEST_ID }));
	}
}